#!/usr/bin/env python
import os, numpy, sys, h5py
from pygrisb.gsolver.qite import qite_mstep_wfn_from_file


qite = qite_mstep_wfn_from_file()
qite.h5save_result()
qite.get_qite_ed_wf_ovlp()
print(f"ansatz 2-qubit gates: {qite._param_expt.get_ansatz_2qubit_gates()}")
