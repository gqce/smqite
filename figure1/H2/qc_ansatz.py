from pyquil.quil import Program
from pyquil.gates import X
from pyquil.experiment._symmetrization import SymmetrizationLevel
from pyquil.experiment._calibration import CalibrationMethod
from pyquil.api import get_qc
from pygrisb.gsolver.forest.util import complete_term_label_list


#qc = get_qc("Aspen-4-2Q-H-qvm")
cq = [0, 1]
shots = 2**10
verbose = False
maxiter = 20
db = 0.4
# ref states
ref_states = [
        [
        Program(X(cq[1]))  # 01
        ],
        ]
# ref_bitstrings should be consistent with ref_states
ref_bitstrings = [
        "10",
        ]
# the ansatz
ansatz_ops = [
        [term for term in complete_term_label_list(2) if term.count("Y")%2==1]
        ]
ansatz_params = [
        list(range(len(terms))) for terms in ansatz_ops
        ]
