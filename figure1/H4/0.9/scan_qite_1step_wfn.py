#!/usr/bin/env python
import os, numpy, sys, h5py, subprocess, shutil
import in_place


dt_list = numpy.arange(0.01, 2.0, 0.05)
e_list = []
for dt in dt_list:
    with in_place.InPlace("../qc_ansatz.py") as f:
        for line in f:
            if "db" in line:
                line = f"db = {dt}\n"
            f.write(line)
    cmd = ["../qite_mstep_wfn.py"]
    if os.path.isfile("result.h5"):
        os.remove("result.h5")
    subprocess.run(cmd)
    with h5py.File("result.h5", "r") as f:
        e_list.append(f["/e_list"][-1])


with open("e_dt.dat", "w") as f:
    for dt, e in zip(dt_list, e_list):
        f.write(f"{dt:.2f} {e:.6f}\n")
