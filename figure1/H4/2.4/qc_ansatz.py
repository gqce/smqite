from pyquil.quil import Program
from pyquil.gates import X
from pyquil.experiment._symmetrization import SymmetrizationLevel
from pyquil.experiment._calibration import CalibrationMethod
from pyquil.api import get_qc
from pygrisb.gsolver.forest.util import complete_term_label_list


#qc = get_qc("Aspen-4-2Q-H")
#qc = get_qc("2q-noisy-qvm")
cq = list(range(6))
shots = 2**10
verbose = False
maxiter = 120
db = 2.5
#error_mitigation = 1
# ref states
ref_states = [
        [
        Program(X(cq[2]), X(cq[5]))  # 001001
        ],
        ]
# ref_bitstrings should be consistent with ref_states
ref_bitstrings = [
        "100100",
        ]
# the ansatz
ansatz_ops = [
        [term for term in complete_term_label_list(6) if term.count("Y")%2==1]
        ]

print(f"ansatz ops: {len(ansatz_ops[0])}")
ansatz_params = [
        list(range(len(terms))) for terms in ansatz_ops
        ]
