import matplotlib.pyplot as plt
import h5py, numpy


with h5py.File("result.h5", "r") as f:
    e_list = f["/e_list"][()]

with h5py.File("../result.h5", "r") as f:
    e_exact = f["/e_exact_list"][0]

beta_list = 0.56*numpy.arange(len(e_list))

plt.plot(beta_list, e_list, "-o")
plt.axhline(y=e_exact, ls="--")
plt.show()
