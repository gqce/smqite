import numpy, h5py
import matplotlib.pyplot as plt


# get H2 data
with h5py.File("H2/0.7/result.h5", "r") as f:
    e_list_h2 = f["/e_list"][()]
with h5py.File("H2/result.h5", "r") as f:
    e_exact_h2 = f["/e_exact_list"][0]

diff = (min(e_list_h2) - e_exact_h2)*627.509
print(f"H2: E_smQITE - e_exact = {diff} kcal/mol.")

dt_e_list_h2 = numpy.loadtxt("H2/0.7/e_dt.dat").T
beta_list_h2 = 0.4*numpy.arange(len(e_list_h2))
n_cnots_h2 = numpy.loadtxt("H2/two_qubit_gates.dat")
n_cnots_list_h2 = [n_cnots_h2*(i+1) for i,_ in enumerate(beta_list_h2)]

print(f"H2: # cnots = {n_cnots_h2}")
diff = (min(dt_e_list_h2[1]) - e_exact_h2)*627.509
print(f"H2: E_1QITE - e_exact = {diff} kcal/mol.")

# get H4 data
with h5py.File("H4/0.9/result.h5", "r") as f:
    e_list_h4 = f["/e_list"][()]
with h5py.File("H4/result.h5", "r") as f:
    e_exact_h4 = f["/e_exact_list"][0]

diff = (min(e_list_h4) - e_exact_h4)*627.509
print(f"H4: E_smQITE - e_exact = {diff} kcal/mol.")

dt_e_list_h4 = numpy.loadtxt("H4/0.9/e_dt.dat").T
beta_list_h4 = 0.56*numpy.arange(len(e_list_h4))
n_cnots_h4 = numpy.loadtxt("H4/two_qubit_gates.dat")
n_cnots_list_h4 = [n_cnots_h4*(i+1) for i,_ in enumerate(beta_list_h4)]

print(f"H4: # cnots = {n_cnots_h4}")
diff = (min(dt_e_list_h4[1]) - e_exact_h4)*627.509
print(f"H4: E_1QITE - e_exact = {diff} kcal/mol.")

fig = plt.figure(figsize=(7.5, 4))

ax1 = plt.subplot(231)
ax1.text(-0.2, 0.9, "(a)", transform=ax1.transAxes)
plt.plot(beta_list_h2, e_list_h2, "-x", label="smQITE")
plt.axhline(y=e_exact_h2, ls="--", label="exact")
ax1.set_xlim(-0.4, 6.2)
plt.ylabel("E (Har.)")
ax1.legend()

ax2 = plt.subplot(232, sharey=ax1)
ax2.text(-0.2, 0.9, "(b)", transform=ax2.transAxes)
ax2.text(0.4, 0.7, r"$H_{2}$", transform=ax2.transAxes)
ax2.text(0.22, 0.4, "single-step\n   QITE", transform=ax2.transAxes)
plt.plot(dt_e_list_h2[0][::4], dt_e_list_h2[1][::4], "r-o")
plt.axhline(y=e_exact_h2, ls="--", label="exact")
#plt.axhline(y=numpy.min(dt_e_list_h2[1]), ls=":")
ax2.set_xlim(-0.1, 1.2)
#plt.ylabel("E (Har.)")

ax3 = plt.subplot(233, sharex=ax1)
ax3.text(-0.2, 0.9, "(c)", transform=ax3.transAxes)
ax3.ticklabel_format(axis='y', style="sci", scilimits=(0,0))
plt.axhline(y=n_cnots_h2, ls="-", label="smQITE")
plt.plot(beta_list_h2, n_cnots_list_h2, "r-^", label="QITE")
ax3.set_ylim(0, 1.4e2)
plt.ylabel("Circuit depth")
ax3.legend()

ax4 = plt.subplot(234, sharex = ax1)
ax4.text(-0.2, 1, "(d)", transform=ax4.transAxes)
plt.plot(beta_list_h4, e_list_h4, "-x", label="smQITE")
#plt.axhline(y=e_list_h4[1], ls=":")
plt.axhline(y=e_exact_h4, ls="--", label="exact")
plt.xlabel(r"$\beta$")
plt.ylabel("E (Har.)")

ax5 = plt.subplot(235, sharey=ax4, sharex=ax2)
ax5.text(-0.2, 1, "(e)", transform=ax5.transAxes)
ax5.text(0.4, 0.7, r"$H_{4}$", transform=ax5.transAxes)
plt.plot(dt_e_list_h4[0][::2], dt_e_list_h4[1][::2], "r-o")
plt.axhline(y=e_exact_h4, ls="--", label="exact")
#plt.axhline(y=numpy.min(dt_e_list_h4[1]), ls=":")
plt.xlabel(r"$\Delta \tau$")
#plt.ylabel("E (Har.)")

ax6 = plt.subplot(236, sharex=ax1)
ax6.text(-0.2, 1, "(f)", transform=ax6.transAxes)
ax6.ticklabel_format(axis='y', style="sci", scilimits=(0,0))
plt.plot(beta_list_h4, n_cnots_list_h4, "r-^")
plt.axhline(y=n_cnots_h4, ls="-")
ax6.set_ylim(0, 1.8e5)
plt.xlabel(r"$\beta$")
plt.ylabel("Circuit depth")

fig.tight_layout()
plt.show()
fig.savefig("Figure1.pdf")
