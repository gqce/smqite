#!/usr/bin/env python
import os, numpy, sys, h5py, subprocess, shutil


rlist = numpy.arange(0.2, 2.5, 0.1)
e_qite_wfn = []
for r in rlist:
    d = f"{r:.1f}"
    os.chdir(d)
    cmd = ["../qite_mstep_wfn.py"]
    subprocess.run(cmd)
    with h5py.File("result.h5", "r") as f:
        e_qite_wfn.append(f["/e_list"][-1])
    os.chdir("..")


with h5py.File("result.h5", "a") as f:
    if "/e_qite_wfn" in f:
        del f["/e_qite_wfn"]
    f["/e_qite_wfn"] = e_qite_wfn
