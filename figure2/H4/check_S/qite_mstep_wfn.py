#!/usr/bin/env python
import os, numpy, sys, h5py
from pygrisb.gsolver.qite import qite_mstep_wfn_from_file
from pyquil.paulis import PauliSum, PauliTerm


qite = qite_mstep_wfn_from_file()
qite.h5save_result()

# load s-square operator
with open("s2.inp", "r") as f:
    sop = [PauliTerm.from_compact_str(label.rstrip("\n").rstrip())
            for label in f if abs(float(label.split("*")[0])) >= 1.e-8]
    sop = PauliSum(sop)

s2_expval = qite.get_expval(sop)
print(s2_expval)
