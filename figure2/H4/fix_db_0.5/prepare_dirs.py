#!/usr/bin/env python

from qiskit.aqua.algorithms import ExactEigensolver
from qiskit.chemistry import FermionicOperator
from qiskit.chemistry.drivers import PySCFDriver, UnitsType
from qiskit.aqua.operators.weighted_pauli_operator import Z2Symmetries
from qiskit.chemistry.components.initial_states import HartreeFock
import numpy, h5py, os


def generate_qubit_hamil(r):
    driver = PySCFDriver(
            atom=(
                    f'H 0 0 0;'
                    f'H {r} 0 0;'
                    f'H {r*2} 0 0;'
                    f'H {r*3} 0 0'
                    ),
            unit=UnitsType.ANGSTROM,
            charge=0,
            spin=0,
            basis='sto3g',
            max_cycle=200,
            )

    molecule = driver.run()
    print(f"nuclear repulsion energy = {molecule.nuclear_repulsion_energy}")
    num_particles = molecule.num_alpha + molecule.num_beta
    num_spin_orbitals = molecule.num_orbitals * 2
    print(f"num_spin_orbitals: {num_spin_orbitals}")

    fermiOp = FermionicOperator(h1=molecule.one_body_integrals,
            h2=molecule.two_body_integrals)
    qubitOp = fermiOp.mapping(map_type='parity')
    qubitOp = Z2Symmetries.two_qubit_reduction(qubitOp, num_particles)

    initial_state = HartreeFock(
            qubitOp.num_qubits,
            num_spin_orbitals,
            num_particles,
            'parity',
            two_qubit_reduction=True,
            )
    print("initial state:", initial_state.bitstr)

    nqubits = qubitOp.num_qubits
    # adding nuclear_repulsion_energy
    istr = "I"*nqubits
    for i, wp in enumerate(qubitOp._paulis):
        label = wp[1].to_label()
        if istr == label:
            qubitOp._paulis[i][0] += molecule.nuclear_repulsion_energy
            break
    with open("h.inp", "w") as f:
        for weight, pauli in qubitOp._paulis:
            if abs(weight) > 1.e-8:
                label = pauli.to_label()
                labels = "".join(f"{s}{i}" for i,s in enumerate(label) \
                        if s != "I")
                if labels == "":
                    labels = "I"
                f.write(f"{weight.real:.8f}*{labels}\n")

    edsolver = ExactEigensolver(qubitOp)
    exact_solution = edsolver.run()

    print("Exact Result:", exact_solution['energy'])
    print("hf energy:", molecule.hf_energy)
    return exact_solution['energy'], molecule.hf_energy


rlist = numpy.arange(0.4, 2.5, 0.1)
e_exact_list = []
e_hf_list = []
for r in rlist:
    d = f"{r:.1f}"
    if not os.path.exists(d):
        os.mkdir(d)
    os.chdir(d)
    e_exact, e_hf = generate_qubit_hamil(r)
    e_exact_list.append(e_exact)
    e_hf_list.append(e_hf)
    os.chdir("..")

with h5py.File("result.h5", "w")  as f:
    f["/r_list"]= rlist
    f["/e_exact_list"] = e_exact_list
    f["/e_hf_list"] = e_hf_list

