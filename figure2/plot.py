import matplotlib.pyplot as plt
import h5py, numpy


# load data
def load_result(fname):
    with h5py.File(fname, "r") as f:
        r_list = f["/r_list"][()]
        e_hf_list = f["/e_hf_list"][()]
        e_exact_list = f["/e_exact_list"][()]
        e_qite_wfn = f["/e_qite_wfn"][()]

    return [r_list, e_hf_list, e_exact_list, e_qite_wfn]

data_h2 = load_result("./H2/result.h5")
data_h4 = load_result("./H4/result.h5")
data_lih = load_result("./LiH/result.h5")
data_hf = load_result("./HF/result.h5")
data = [data_h2, data_h4, data_lih, data_hf]
names = [r"$H_2$", r"$H_4$", r"$LiH$", r"$HF$"]

fig = plt.figure(constrained_layout=True, figsize=(7.5, 4))
widths = [2.5, 2.5, 2.5, 2.5]
heights = [2, 1]
spec5 = fig.add_gridspec(ncols=len(widths), nrows=len(heights),
        width_ratios=widths, height_ratios=heights)
labels = [["a", "b", "c", "d"], ["e", "f", "g", "h"]]
for row in range(2):
    for col in range(4):
        if row == 0:
            ax = fig.add_subplot(spec5[row, col])
            ax.annotate(f"({labels[row][col]})", (0.7, 0.93),
                    xycoords='axes fraction', va='center')
            plt.plot(data[col][0], data[col][1], color="gray",
                    label="HF")
            if col == 2:
                plt.plot(data[col][0][::2], data[col][3][::2], "x",
                        label="smQITE")
            else:
                plt.plot(data[col][0], data[col][3], "x", label="smQITE")
            plt.plot(data[col][0], data[col][2], label="Exact")
            ax.annotate(names[col], (0.3, 0.5), xycoords='axes fraction',
                    va='center')
            if col == 0:
                ax.set_ylabel("E (Ha)")
            if col == 2:
                plt.legend(framealpha=0.0)
        else:
            if col == 0:
                ax = ax1 = fig.add_subplot(spec5[row, col])
                ax1.set_ylabel("Error (kcal/mol)")
                ax1.set_ylim(1.e-7, 2)
            else:
                ax = fig.add_subplot(spec5[row, col], sharey=ax1)
            ax.annotate(f"({labels[row][col]})", (0.2, 0.8),
                    xycoords='axes fraction', va='center')
            err = 627.509*abs(data[col][2]-data[col][3])
            plt.plot(data[col][0], err, "x")
            plt.yscale("log")
            ax.set_xlabel(r"R ($\AA$)")
            ax.set_yticks([1, 1.e-1, 1.e-4, 1.e-7])
            ax.axhline(1, ls=":")
plt.show()
fig.savefig("Figure2.pdf")
