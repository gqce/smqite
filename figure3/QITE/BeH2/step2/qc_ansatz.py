from pyquil.quil import Program
from pyquil.gates import X
from pyquil.api import get_qc
from pyquil.paulis import PauliTerm, exponential_map
from pygrisb.gsolver.forest.util import get_term_labels_from_file


#qc = get_qc("Aspen-4-2Q-H")
#qc = get_qc("2q-noisy-qvm")
cq = list(range(8))
shots = 2**10
verbose = False
maxiter = 30
db = 10
auto_tune_step = True
continuous_run = True
init_eval = 1
#error_mitigation = 1
# ref states
ref_states = [
        [
        Program(X(cq[3]), X(cq[7]))
        ],
        ]
# ref_bitstrings should be consistent with ref_states
ref_bitstrings = [
        "10001000",
        ]
# the ansatz
terms = get_term_labels_from_file("qc_ansatz_1.inp")
ansatz_ops = [
        terms,
        ]
print(f"ansatz ops: {len(ansatz_ops[0])}")
ansatz_params = [
        list(range(len(terms))) for terms in ansatz_ops
        ]

# ref states
from pygrisb.gsolver.forest._ansatz import reorder_digits
init_ansatz = Program(X(cq[3]), X(cq[7]))
import h5py
with h5py.File("param_ansatz.h5", "r") as f:
    params = f["/a_db"][()]

term_labels = [reorder_digits(label, cq) for label in ansatz_ops[0]]
pauli_terms = [PauliTerm.from_compact_str(label) for label in term_labels]

for term, coef in zip(pauli_terms, params):
    init_ansatz += exponential_map(term)(-2*coef)
ref_states = [init_ansatz]
