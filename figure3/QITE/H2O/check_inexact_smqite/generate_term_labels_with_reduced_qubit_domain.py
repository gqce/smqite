from pygrisb.gsolver.forest.util import complete_term_label_list

nq = 8
nsize = 4

with open("qc_ansatz_1.inp", "w") as f:
    for term in complete_term_label_list(nq):
        term = term[2:]  # remove 1*
        if len(term) <= nsize*2 and term.count("Y")%2==1:
            f.write(f"{term}\n")
