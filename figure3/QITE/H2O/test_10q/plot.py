import h5py, numpy
import matplotlib.pyplot as plt


with h5py.File("result.h5", "r") as f:
    r_list = f["/r_list"][()]
    e_exact_list = f["/e_exact_list"][()]
    e_hf_list = f["/e_hf_list"][()]
#    e_qite_wfn = f["/e_qite_wfn"][()]


#print(f"max error: {numpy.max(numpy.abs(e_qite_wfn-e_exact_list))}")
plt.plot(r_list, e_hf_list)
plt.plot(r_list, e_exact_list)
#plt.plot(r_list, e_qite_wfn, "o")
plt.show()
