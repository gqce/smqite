#!/usr/bin/env python

from qiskit.aqua.algorithms import ExactEigensolver
from qiskit.chemistry import FermionicOperator
from qiskit.chemistry.drivers import PySCFDriver, UnitsType
from qiskit.aqua.operators.weighted_pauli_operator import Z2Symmetries, \
        WeightedPauliOperator
from qiskit.chemistry.components.initial_states import HartreeFock
from qiskit.chemistry.components.variational_forms import UCCSD
import numpy, h5py, os


def generate_qubit_hamil(r):
    z0 = 0.11779
    z1 = 0.47116
    y0 = 0.75545
    r0 = numpy.sqrt(y0**2 + (z0+z1)**2)
    ratio = r/r0
    driver = PySCFDriver(
            atom=(
                    f'O 0 0 {z0*ratio};'
                    f'H 0 {y0*ratio} {-z1*ratio};'
                    f'H 0 {-y0*ratio} {-z1*ratio};'
                    ),
            unit=UnitsType.ANGSTROM,
            charge=0,
            spin=0,
            basis='sto3g',
            max_cycle=200,
            )

    molecule = driver.run()
    freeze_list = [0]
    remove_list = []
    num_particles = molecule.num_alpha + molecule.num_beta
    num_spin_orbitals = molecule.num_orbitals * 2
    remove_list = [x % molecule.num_orbitals for x in remove_list]
    freeze_list = [x % molecule.num_orbitals for x in freeze_list]
    remove_list = [x - len(freeze_list) for x in remove_list]
    remove_list += [x + molecule.num_orbitals - len(freeze_list)
            for x in remove_list]
    freeze_list += [x + molecule.num_orbitals for x in freeze_list]
    ferOp = FermionicOperator(h1=molecule.one_body_integrals,
            h2=molecule.two_body_integrals)
    ferOp, energy_shift = ferOp.fermion_mode_freezing(freeze_list)
    num_spin_orbitals -= len(freeze_list)
    num_particles -= len(freeze_list)
    ferOp = ferOp.fermion_mode_elimination(remove_list)
    num_spin_orbitals -= len(remove_list)
    print(f"num_spin_orbitals: {num_spin_orbitals}")

    qubitOp = ferOp.mapping(map_type='parity', threshold=1.e-8)
    qubitOp = Z2Symmetries.two_qubit_reduction(qubitOp, num_particles)

    initial_state = HartreeFock(
            qubitOp.num_qubits,
            num_spin_orbitals,
            num_particles,
            'parity',
            two_qubit_reduction=True,
            )
    print("initial state:", initial_state.bitstr)

    nqubits = qubitOp.num_qubits
    # adding nuclear_repulsion_energy
    istr = "I"*nqubits
    for i, wp in enumerate(qubitOp._paulis):
        label = wp[1].to_label()
        if istr == label:
            qubitOp._paulis[i][0] += molecule.nuclear_repulsion_energy + \
                    energy_shift
            break
    with open("h.inp", "w") as f:
        for weight, pauli in qubitOp._paulis:
            if abs(weight) > 1.e-8:
                label = pauli.to_label()
                labels = "".join(f"{s}{i}" for i,s in enumerate(label) \
                        if s != "I")
                if labels == "":
                    labels = "I"
                f.write(f"{weight.real:.8f}*{labels}\n")

    edsolver = ExactEigensolver(qubitOp)
    exact_solution = edsolver.run()

    var_form = UCCSD(
        qubitOp.num_qubits,
        depth=1,
        num_orbitals=num_spin_orbitals,
        num_particles=num_particles,
        initial_state=initial_state,
        qubit_mapping='parity',
        two_qubit_reduction=True,
    )
    ansatz_ops = [WeightedPauliOperator(hop._paulis) for hop  \
            in var_form._hopping_ops]
    ansatz_labels = []
    for hop in var_form._hopping_ops:
        for pauli in hop._paulis[:1]:
            label = pauli[1].to_label()
            # check Y numbers
            num_y = label.count("Y")
            if num_y %2 == 0:
                continue
            labels = "".join(f"{s}{i}" for i,s in enumerate(label) \
                    if s != "I")
            if labels == "1 ":
                labels = "I"
            if labels in ansatz_labels:
                print(f"{s} appeared more than once.")
            else:
                ansatz_labels.append(labels)
    with open("qc_ansatz_1.inp", "w") as f:
        for label in ansatz_labels:
            f.write(f"{label}\n")


    print("Exact Result:", exact_solution['energy'])
    print("hf energy:", molecule.hf_energy)
    return exact_solution['energy'], molecule.hf_energy


rlist = numpy.arange(0.6, 2.5, 0.1)
e_exact_list = []
e_hf_list = []
for r in rlist:
    d = f"{r:.1f}"
    if not os.path.exists(d):
        os.mkdir(d)
    os.chdir(d)
    e_exact, e_hf = generate_qubit_hamil(r)
    e_exact_list.append(e_exact)
    e_hf_list.append(e_hf)
    os.chdir("..")

with h5py.File("result.h5", "w")  as f:
    f["/r_list"]= rlist
    f["/e_exact_list"] = e_exact_list
    f["/e_hf_list"] = e_hf_list

