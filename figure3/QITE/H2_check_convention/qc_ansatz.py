from pyquil.quil import Program
from pyquil.gates import X
from pyquil.experiment._symmetrization import SymmetrizationLevel
from pyquil.experiment._calibration import CalibrationMethod
from pyquil.api import get_qc
from pygrisb.gsolver.forest.util import get_term_labels_from_file


#qc = get_qc("Aspen-4-2Q-H")
#qc = get_qc("2q-noisy-qvm")
cq = list(range(2))
shots = 2**10
verbose = False
maxiter = 80
db = 0.5
#error_mitigation = 1
# ref states
ref_states = [
        [
        Program(X(cq[1]))  # 00101101
        ],
        ]
# ref_bitstrings should be consistent with ref_states
ref_bitstrings = [
        "10",
        ]
# the ansatz
terms = get_term_labels_from_file("qc_ansatz_1.inp")
ansatz_ops = [
        terms,
        ]
print(f"ansatz ops: {len(ansatz_ops[0])}")
ansatz_params = [
        list(range(len(terms))) for terms in ansatz_ops
        ]
