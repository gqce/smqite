from pygrisb.gsolver.forest.util import complete_term_label_list

nsize = 4

with open("qc_ansatz_1.inp", "w") as f:
    for term in complete_term_label_list(6):
        term = term[2:]  # remove 1*
        if len(term) <= nsize*2:
            f.write(f"{term}\n")
