#!/usr/bin/env python
import os, numpy, sys, h5py, subprocess, shutil


rlist = numpy.arange(0.4, 2.5, 0.1)
e_qite_wfn = []
for r in rlist:
    d = f"{r:.1f}"
    print(f"dir {d}")
    os.chdir(d)
    with h5py.File("result.h5", "r") as f:
        elist = f["/e_list"][-10:]
        e_qite_wfn.append(numpy.mean(elist))
    os.chdir("..")


with h5py.File("result.h5", "a") as f:
    if "/e_qite_wfn" in f:
        del f["/e_qite_wfn"]
    f["/e_qite_wfn"] = e_qite_wfn
