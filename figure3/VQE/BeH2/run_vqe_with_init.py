#!/usr/bin/env python

from qiskit.aqua.algorithms import VQE, ExactEigensolver
from qiskit import IBMQ, BasicAer
from qiskit.aqua.components.optimizers import SLSQP, COBYLA
from qiskit.chemistry import FermionicOperator
from qiskit.chemistry.drivers import PySCFDriver, UnitsType
from qiskit.aqua.operators.weighted_pauli_operator import Z2Symmetries
from qiskit.chemistry.components.initial_states import HartreeFock
from qiskit.chemistry.components.variational_forms import UCCSD
import numpy, h5py, os


def generate_qubit_hamil(r):
    driver = PySCFDriver(
            atom=(
                    f'Be 0 0 0;'
                    f'H {-r} 0 0;'
                    f'H {r} 0 0;'
                    ),
            unit=UnitsType.ANGSTROM,
            charge=0,
            spin=0,
            basis='sto3g',
            max_cycle=200,
            )

    molecule = driver.run()
    freeze_list = [0]
    remove_list = [-3]
    num_particles = molecule.num_alpha + molecule.num_beta
    num_spin_orbitals = molecule.num_orbitals * 2
    remove_list = [x % molecule.num_orbitals for x in remove_list]
    freeze_list = [x % molecule.num_orbitals for x in freeze_list]
    remove_list = [x - len(freeze_list) for x in remove_list]
    remove_list += [x + molecule.num_orbitals - len(freeze_list)
            for x in remove_list]
    freeze_list += [x + molecule.num_orbitals for x in freeze_list]
    ferOp = FermionicOperator(h1=molecule.one_body_integrals,
            h2=molecule.two_body_integrals)
    ferOp, energy_shift = ferOp.fermion_mode_freezing(freeze_list)
    num_spin_orbitals -= len(freeze_list)
    num_particles -= len(freeze_list)
    ferOp = ferOp.fermion_mode_elimination(remove_list)
    num_spin_orbitals -= len(remove_list)
    print(f"num_spin_orbitals: {num_spin_orbitals}")

    qubitOp = ferOp.mapping(map_type='parity', threshold=1.e-8)
    qubitOp = Z2Symmetries.two_qubit_reduction(qubitOp, num_particles)

    initial_state = HartreeFock(
            qubitOp.num_qubits,
            num_spin_orbitals,
            num_particles,
            'parity',
            two_qubit_reduction=True,
            )
    print("initial state:", initial_state.bitstr)

    nqubits = qubitOp.num_qubits
    # adding nuclear_repulsion_energy
    istr = "I"*nqubits
    for i, wp in enumerate(qubitOp._paulis):
        label = wp[1].to_label()
        if istr == label:
            qubitOp._paulis[i][0] += molecule.nuclear_repulsion_energy \
                    + energy_shift
            break
    with open("h.inp", "w") as f:
        for weight, pauli in qubitOp._paulis:
            if abs(weight) > 1.e-8:
                label = pauli.to_label()
                labels = "".join(f"{s}{i}" for i,s in enumerate(label) \
                        if s != "I")
                if labels == "":
                    labels = "I"
                f.write(f"{weight.real:.8f}*{labels}\n")

    edsolver = ExactEigensolver(qubitOp)
    exact_solution = edsolver.run()

    var_form = UCCSD(
            qubitOp.num_qubits,
            depth=1,
            num_orbitals=num_spin_orbitals,
            num_particles=num_particles,
            initial_state=initial_state,
            qubit_mapping='parity',
            two_qubit_reduction=True,
            )
    # choose only
    for i, hop in enumerate(var_form._hopping_ops):
        var_form._hopping_ops[i]._basis = hop._basis[:1]
        var_form._hopping_ops[i]._paulis = hop._paulis[:1]
        var_form._hopping_ops[i]._paulis[0][0] = 1.j

    backend = BasicAer.get_backend("statevector_simulator")
    optimizer = SLSQP(maxiter=1)
    # optimizer = COBYLA(maxiter=1500, tol=0.0001)

    # initial guess
    with h5py.File("result.h5", "r") as f:
        params = f["/a_db"][()]

    vqe = VQE(qubitOp, var_form, optimizer, initial_point=-2*params)
    res = vqe.run(backend)
    print(res["num_optimizer_evals"])
    vqe_energy = res['energy']
    print("Exact Result:", exact_solution['energy'])
    print("hf energy:", molecule.hf_energy)
    print("vqe energy:", vqe_energy)
    with open("result.dat", "w") as f:
        f.write(f"{vqe_energy}, {res['num_optimizer_evals']}")
    return exact_solution['energy'], molecule.hf_energy, vqe_energy


IBMQ.load_account()
rlist = [3.8]
e_exact_list = []
e_hf_list = []
e_vqe_list = []
for r in rlist:
    d = f"{r:.1f}_init"
    if not os.path.exists(d):
        os.mkdir(d)
    os.chdir(d)
    e_exact, e_hf, e_vqe = generate_qubit_hamil(r)
    e_exact_list.append(e_exact)
    e_hf_list.append(e_hf)
    e_vqe_list.append(e_vqe)
    os.chdir("..")
