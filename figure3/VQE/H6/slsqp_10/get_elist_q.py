#!/usr/bin/env python
import os, numpy, sys, h5py, subprocess, shutil


rlist = numpy.arange(0.4, 2.5, 0.1)
e_vqe_list = []
for r in rlist:
    d = f"{r:.1f}"
    print(f"dir {d}")
    os.chdir(d)
    with open("result.dat", "r") as f:
        e = float(f.readline().split(",")[0])
        e_vqe_list.append(e)
    os.chdir("..")


with h5py.File("result.h5", "a") as f:
    if "/e_vqe_list" in f:
        del f["/e_vqe_list"]
    f["/e_vqe_list"] = e_vqe_list
