import h5py, numpy
import matplotlib.pyplot as plt


with h5py.File("result.h5", "r") as f:
    r_list = f["/r_list"][()]
    e_exact_list = f["/e_exact_list"][()]
    e_hf_list = f["/e_hf_list"][()]
    e_vqe_list = f["/e_vqe_list"][()]


print(f"max diff: {numpy.max(numpy.abs(e_vqe_list - e_exact_list))}")
plt.plot(r_list, e_hf_list)
plt.plot(r_list, e_exact_list)
plt.plot(r_list, e_vqe_list, "o")
plt.show()
