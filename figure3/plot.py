import matplotlib.pyplot as plt
import h5py, numpy


# load data
def load_result(fname):
    with h5py.File(f"./QITE/{fname}", "r") as f:
        r_list = f["/r_list"][()]
        e_hf_list = f["/e_hf_list"][()]
        e_exact_list = f["/e_exact_list"][()]
        e_qite_wfn = f["/e_qite_wfn"][()]
    with h5py.File(f"./VQE/{fname}", "r") as f:
        e_vqe_list = f["/e_vqe_list"][()]

    return [r_list, e_hf_list, e_exact_list, e_qite_wfn, e_vqe_list]

data_h2o = load_result("./H2O/result.h5")
print("h2o: e_qite-vqe:",
        min(data_h2o[3]-data_h2o[4]), max(data_h2o[3]-data_h2o[4]))
print(" ".join(f"{x:.4f}" for x in data_h2o[3]-data_h2o[4]))
print(f"max error-qite: {max(data_h2o[3]-data_h2o[2])}")
print(f"max error-veq: {max(data_h2o[4]-data_h2o[2])}")

data_beh2 = load_result("./BeH2/result.h5")
print("beh2: e_qite-vqe:",
        min(data_beh2[3]-data_beh2[4]), max(data_beh2[3]-data_beh2[4]))
print(" ".join(f"{x:.4f}" for x in data_beh2[3]-data_beh2[4]))
print(f"max error-qite: {max(data_beh2[3]-data_beh2[2])}")
print(f"max error-vqe: {max(data_beh2[4]-data_beh2[2])}")

data_h6 = load_result("./H6/result.h5")
print("h6: e_qite-vqe:",
        min(data_h6[3]-data_h6[4]), max(data_h6[3]-data_h6[4]))
print(" ".join(f"{x:.4f}" for x in data_h6[3]-data_h6[4]))
print(f"max error-qite: {max(data_h6[3]-data_h6[2])}")
print(f"max error-vqe: {max(data_h6[4]-data_h6[2])}")

data = [data_h2o, data_beh2, data_h6]
names = [r"$H_{2}O$", r"$BeH_2$", r"$H_6$"]

fig = plt.figure(constrained_layout=True, figsize=(7.5, 4))
widths = [2.5, 2.5, 2.5]
heights = [2, 1]
spec = fig.add_gridspec(ncols=len(widths), nrows=len(heights),
        width_ratios=widths, height_ratios=heights)
labels = [["a", "b", "c"], ["d", "e", "f"]]
rlabels = [r"$R_{O-H}$", r"$R_{Be-H}$", r"$R_{H-H}$"]
for row in range(2):
    for col in range(3):
        if row == 0:
            ax = fig.add_subplot(spec[row, col])
            plt.plot(data[col][0], data[col][1], color="gray",
                    label="HF")
            plt.plot(data[col][0], data[col][3], "x", label="smQITE")
            plt.plot(data[col][0], data[col][4], "+", label="VQE")
            plt.plot(data[col][0], data[col][2], label="Exact")
            ax.annotate(names[col], (0.3, 0.4), xycoords='axes fraction',
                    va='center')
            if col == 0:
                ax.set_ylabel("E (Ha)")
            if col == 1:
                plt.legend()
        else:
            if col == 0:
                ax = ax1 = fig.add_subplot(spec[row, col])
                ax1.set_ylabel("Error (kcal/mol)")
                # ax1.set_ylim(1.e-7, 10)
            else:
                ax = fig.add_subplot(spec[row, col], sharey=ax1)
            err = 627.509*abs(data[col][2]-data[col][3])
            plt.plot(data[col][0], err, "x")
            err = 627.509*abs(data[col][2]-data[col][4])
            plt.plot(data[col][0], err, "+")
            plt.yscale("log")
            ax.set_xlabel(f"{rlabels[col]} ($\AA$)")
            # ax.set_yticks([1, 1.e-1, 1.e-4, 1.e-7])
            ax.axhline(1, ls=":")
        ax.annotate(f"({labels[row][col]})", (0.15, 0.8),
                xycoords='axes fraction', va='center')

plt.show()
fig.savefig("Figure3.pdf")
