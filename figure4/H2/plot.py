import h5py, numpy
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.inset_locator import InsetPosition

with h5py.File("./wfn/h2_result.h5", "r") as f:
    r_list = f["/r_list"][()]
    e_exact_list = f["/e_exact_list"][()]
    e_hf_list = f["/e_hf_list"][()]
    e_qite_wfn = f["/e_qite_wfn"][()]

with h5py.File("./qc/h2_result.h5", "r") as f:
    r_list2 = f["/r_list"][()]
    e_qite_qc = f["/e_qite_qc"][()]
    e_qite_qc_err = f["/e_qite_qc_err"][()]

err = e_qite_qc - e_exact_list[::2]
print(f"err: [{min(err)}, {max(err)}]")

with h5py.File("qc/2.4/result.h5", "r") as f:
    qc_es = f["/e_list"][()]

with h5py.File("wfn/2.4/result.h5", "r") as f:
    wfn_es = f["/e_list"][()]
betas = 0.5*numpy.arange(len(qc_es))

fig, ax1 = plt.subplots(figsize=(4,4))
ax1.plot(r_list, e_hf_list, label="HF")
ax1.plot(r_list, e_exact_list, label="Exact")
ax1.plot(r_list, e_qite_wfn, "gx", label="smQITE-simulation")
ax1.errorbar(r_list2, e_qite_qc, yerr=e_qite_qc_err, fmt="r.",
        label="smQITE-device")
ax1.set_xlabel("R($\AA$)")
ax1.set_ylabel("E(Ha)")
plt.legend(prop={'size': 8}, ncol=2)
# Create a set of inset Axes: these should fill the bounding box allocated to
# them.
ax2 = plt.axes([0,0,1,1])
# Manually set the position and relative size of the inset axes within ax1
ip = InsetPosition(ax1, [0.3,0.4,0.55,0.4])
ax2.set_axes_locator(ip)
ax2.set_xlabel(r"$\beta$")
ax2.plot(betas, wfn_es, "gx")
ax2.plot(betas, qc_es, "r.")

#fig.tight_layout()
plt.show()
fig.savefig("Figure4.pdf")
