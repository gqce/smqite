#!/usr/bin/env python
import os, numpy, sys, h5py, subprocess, shutil


rlist = numpy.arange(0.2, 2.5, 0.2)
e_qite_qc = []
e_qite_qc_err = []
for r in rlist:
    d = f"{r:.1f}"
    print(f"dir {d}")
    os.chdir(d)
    with h5py.File("result.h5", "r") as f:
        elist = f["/e_list"][-10:]
        e_qite_qc.append(numpy.mean(elist))
        e_qite_qc_err.append(numpy.std(elist))
        # e_qite_qc_err.append((numpy.max(elist) - numpy.min(elist))/2)
    os.chdir("..")


with h5py.File("h2_result.h5", "a") as f:
    if "/e_qite_qc" in f:
        del f["/e_qite_qc"]
    f["/e_qite_qc"] = e_qite_qc
    if "/e_qite_qc_err" in f:
        del f["/e_qite_qc_err"]
    f["/e_qite_qc_err"] = e_qite_qc_err
