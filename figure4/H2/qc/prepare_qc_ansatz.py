#!/usr/bin/env python

import numpy, h5py, os

lines_src = open("qc_ansatz.py", "r").readlines()

rlist = numpy.arange(0.2, 2.5, 0.2)
db_list = [0.2]*3 + [0.5]*(len(rlist) - 3)

for i, r in enumerate(rlist):
    d = f"{r:.1f}"
    if not os.path.exists(d):
        os.mkdir(d)
    with open(f"./{d}/qc_ansatz.py", "w") as f:
        for line in lines_src:
            if "db =" in line:
                line = f"db = {db_list[i]}\n"
            f.write(line)
