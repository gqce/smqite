from pygrisb.run.timing import timeit
import numpy, h5py, os
from pyquil.paulis import PauliTerm
import qc_ansatz as qca
from pygrisb.gsolver.forest._ansatz import input_ansatz
from pygrisb.gsolver.forest.util import complete_term_label_list


class qite_base:
    '''
    base class of qite.
    '''
    def __init__(self,
            hop,  # list of pauli terms for the hamiltonian
            imp=0,  # impurity index
            db=0.5,  # time step size
            maxiter=40,  # max time steps
            ):
        self._hop = hop
        self._imp = imp
        self._db = db
        self._maxiter = maxiter
        self._elist = []
        self._a = None
        self._param_expt = None
        self.file_input()

    def file_input(self):
        # order ocnvention is reversed.
        self._istate = qca.ref_bitstrings[self._imp][::-1]
        self._nq = len(self._istate)
        self._ansatz_ops = [PauliTerm.from_compact_str(label) for label in
                qca.ansatz_ops[self._imp]] # not tied to specific qubit order
        if hasattr(qca, "ansatz_ops_sub_indices"):
            self._ansatz_ops_sub_indices =  \
                    qca.ansatz_ops_sub_indices[self._imp]
        else:
            self._ansatz_ops_sub_indices = None
        if hasattr(qca, "maxiter"):
            self._maxiter = qca.maxiter
        if hasattr(qca, "db"):
            self._db = qca.db

    def get_updated_a(self, delta=1.e-4):
        '''update a-coefficients.
        '''
        s = self.get_s()
        b = self.get_b()
        # with regularization
        smat = numpy.identity(s.shape[0], dtype=numpy.float)*delta
        smat += s + s.T
        res = numpy.linalg.lstsq(smat, -b, rcond=-1)
        return res[0]

    def get_s(self):
        return self._s

    def get_b(self):
        return self._b

    def get_expval_init_ps(self, term):
        '''
        get expectation value of pauli terms with respect to the initial
        simple product state (0011 ...).
        '''
        res = term.coefficient
        if abs(res) < 1.e-8:
            res = 0
        else:
            if "X" in term._ops.values() or "Y" in term._ops.values():
                return 0  # zero in simple product state
            for i in term._ops:  # it must be Z
                if self._istate[i] == "1":
                    res *= -1
        return res

    def set_init_e(self):
        '''
        calculate the initial energy of the simple product state.
        '''
        e = 0
        for term in self._hop:
            e += self.get_expval_init_ps(term)
        self._e = e.real
        self._elist.append(self._e)
        print(f"intial state energy: {self._e}")

    def h5save_result(self, fname="result.h5"):
        with h5py.File(fname, "w") as f:
            f["/e_list"] = self._elist
            f["/a"] = self._a


class qite_cstep(qite_base):
    '''
    combined-unitary qite.
    '''
    def file_input(self):
        super().file_input()
        # set somplete list of pauli terms
        n = 4**self._nq
        if n == len(self._ansatz_ops):
            self._full_term_list = self._ansatz_ops
        else:
            self._full_term_list = [PauliTerm.from_compact_str(label)
                    for label in complete_term_label_list(self._nq)]

    @timeit
    def set_init_arrays(self):
        # S_IJ mapping indices
        nterms = len(self._ansatz_ops)
        # dimension for the pauli terms used for evolving the state
        if self._ansatz_ops_sub_indices is None:
            nterms_sub = nterms
        else:
            nterms_sub = len(self._ansatz_ops_sub_indices)
            print(f"dim of ansatz pauli terms: {nterms}, subset: {nterms_sub}")

        try:
            # if prestored, avoid recalculation.
            with h5py.File("sbh_index.h5", "r") as f:
                self._ij0_s = f["/ij0_s"][()]
                assert(nterms == self._ij0_s.shape[0])
                self._ij1_s = f["/ij1_s"][()]
                self._hi0_b = f["/hi0_b"][()]
                self._hi1_b = f["/hi1_b"][()]
                self._i_h = f["/i_h"][()]
        except:
            print("sbh_index.h5 not exist or shape not match.")
            print("setting s_ij")
            self._ij0_s = numpy.zeros([nterms, nterms], dtype=numpy.int)
            self._ij1_s = numpy.zeros([nterms, nterms], dtype=numpy.complex)
            for i, termi in enumerate(self._ansatz_ops):
                if i%200 == 0:
                    print(f"generating group {i}.")
                for j, termj in enumerate(self._ansatz_ops[:i+1]):
                    termij = termi*termj
                    self._ij0_s[i, j] = get_pauli_term_index(termij, self._nq)
                    self._ij1_s[i, j] = termij.coefficient
            print("setting i_b")
            # h*term index
            ntermh = len(self._hop)
            self._hi0_b = numpy.zeros([ntermh, nterms], dtype=numpy.int)
            self._hi1_b = numpy.zeros([ntermh, nterms], dtype=numpy.complex)
            for h, termh in enumerate(self._hop):
                for i, termi in enumerate(self._ansatz_ops):
                    termhi = termh*termi
                    self._hi0_b[h, i] = get_pauli_term_index(termhi, self._nq)
                    self._hi1_b[h, i] = termhi.coefficient
            # h index
            self._i_h = [get_pauli_term_index(termh, self._nq)
                    for termh in self._hop]
            with h5py.File("sbh_index.h5", "w") as f:
                f["/ij0_s"] = self._ij0_s
                f["/ij1_s"] = self._ij1_s
                f["/hi0_b"] = self._hi0_b
                f["/hi1_b"] = self._hi1_b
                f["/i_h"] = self._i_h

        # <\Psi_n | \sigma_I^\dagger \sigma_J | \Psi_n>
        self._s = numpy.zeros([nterms, nterms], dtype=numpy.float)
        # i c_n^(-1/2)<\Psi_n|H \sigma_I|\Psi_n> + c.c.
        self._b = numpy.zeros([nterms], dtype=numpy.float)
        # A = \sum_I a_I \sigma_I
        self._a = numpy.zeros([nterms_sub], dtype=numpy.float)

    def set_init_expvals(self):
        self._full_term_expvals = [self.get_expval_init_ps(term)
                for term in self._full_term_list]

    def set_e(self):
        e = sum([self._full_term_expvals[self._i_h[i]]*term.coefficient \
                for i, term in enumerate(self._hop)])
        self._e = e.real
        self._elist.append(self._e)

    @timeit
    def set_sb(self):
        nterms = len(self._ansatz_ops)
        ntermh = len(self._hop)
        for i in range(nterms):
            for j in range(i+1):
                ij, coefij = self._ij0_s[i, j], self._ij1_s[i, j]
                zes = coefij*self._full_term_expvals[ij]
                self._s[i, j] = zes.real
                if i != j:
                    self._s[j, i] = self._s[i, j]
            bi = 0
            for h in range(ntermh):
                hi, coef = self._hi0_b[h, i], self._hi1_b[h, i]
                zes = coef*self._full_term_expvals[hi]
                bi -= zes.imag*2
            self._b[i] = bi
        cn = 1 - 2*self._db*self._e
        assert(cn > 0), f"cn = {cn} <0!"
        self._b /= numpy.sqrt(cn)

    @timeit
    def one_iteration(self, iter):
        self.set_sb()
        a_new = self.get_updated_a()
        if self._ansatz_ops_sub_indices is not None:
            a_new = a_new[self._ansatz_ops_sub_indices]
        self._a += a_new
        self.set_expvals()
        self.set_e()
        print(f"{iter:2d}: e = {self._e}")


    @timeit
    def run(self, nswitch=0, tol=1.e-6):
        """
        main routine.
        """
        self.set_init_arrays()
        self.set_init_expvals()
        self.set_e()
        print(f"init: e = {self._e}")
        for i in range(self._maxiter):
            self.one_iteration(i)
        self.h5save_result()


class qite_cstep_wfn(qite_cstep):
    '''
    using wavefunction simulator
    '''
    def file_input(self):
        super().file_input()
        from pygrisb.gsolver.forest.paramexpt import paramExperimentWFN
        self._param_expt = paramExperimentWFN(
                hamiltonian=self._hop,
                )
        # check exact energy
        print('Exact ground state energy:', self._param_expt.get_exact_gs())
        # load initial state, by convention, cq already applied
        self._param_expt.set_ref_state(qca.ref_states[self._imp])
        print('initial circuit energy estimate: ',
                self._param_expt.objective_function())

        # load ansatz, no custom_qubits needed
        ansatz_circuit = input_ansatz(self._imp)
        self._param_expt.set_ansatz(ansatz_circuit)

    @timeit
    def set_expvals(self):
        memory_map = {'theta': self._a*self._db*(-2)}
        self._full_term_expvals = self._param_expt.evaluate_expval(memory_map,
               terms=self._full_term_list)


class qite_cstep_qc(qite_cstep):
    '''
    using wavefunction simulator
    '''
    def file_input(self):
        super().file_input()
        from pygrisb.gsolver.forest.paramexpt import  \
                paramExperimentQCFullExpval
        self._param_expt = paramExperimentQCFullExpval(
                qc=qca.qc,
                hamiltonian=self._hop,
                shots=qca.shots,
                custom_qubits=qca.cq,
                verbose=qca.verbose,
                )
        # load initial state
        self._param_expt.set_ref_state(qca.ref_states[self._imp])
        # load ansatz
        ansatz_circuit = input_ansatz(self._imp)
        self._param_expt.set_ansatz(ansatz_circuit)
        # cq_appled pauli terms for experiments
        terms = [PauliTerm.from_compact_str(label)
                for label in complete_term_label_list(self._nq, cq=qca.cq)]
        self._param_expt.compile_tomo_expts(pauli_list=terms)
        self._param_expt.set_term_es(numpy.zeros(4**self._nq))

    def set_expvals(self):
        memory_map = {'theta': self._a*self._db*(-2)}
        self._param_expt.run_experiments(memory_map)
        self._full_term_expvals = self._param_expt.term_es


class qite_1step(qite_base):
    '''
    one-step qite.
    '''
    def __init__(self,
            hop,  # list of pauli terms for the hamiltonian
            imp=0,  # impurity index
            db=0.5,  # time step size
            maxiter=40,  # max time steps
            ):
        super().__init__(
                hop,
                imp=imp,
                db=db,
                maxiter=maxiter,
                )
        self._s = None  # <\Psi_0 | \sigma_I^\dagger \sigma_J | \Psi_0>
        self._bi = None  # <\Psi_0|H \sigma_I|\Psi_0>, imaginary part only
        self._a = None  # A = \sum_I a_I \sigma_I

    def run(self, nswitch=0, tol=1.e-6):
        """
        main routine.
        """
        self.setup_init_sb()
        self.set_init_e()
        # for restarting with better point.
        from collections import deque
        e_best = deque(maxlen=2)
        e_best.append(self._e)
        a_best = deque(maxlen=2)
        a_best.append(None)
        e_choice = self._e
        a_choice = None
        e_last = self._e
        nchk = 0  # check how many consecutive times for not decreasing energy
        for i in range(self._maxiter):
            self._a = self.get_updated_a()
            # evaluate expectation values, including H and others if needed.
            self.evaluate_e()
            print(f"{i:2d}: e = {self._e}")
            if e_choice > self._e:
                e_choice = self._e
                a_choice = self._a
            if e_best[-1] > self._e:
                e_best.append(self._e)
                a_best.append(self._a)
                nchk = 0
            else:
                nchk += 1
                if nchk > nswitch:
                    diff = e_last - e_best[-1]
                    print(f"db step error: {diff}")
                    if abs(diff) < tol:
                        break
                    else:
                        e_last = e_best[-1]
                        nchk = 0
                        # move to smaller step size.
                        if a_best[0] is not None:
                            a_best[0] *= 2
                        self._db /= 2
                        self._a = a_best[0]
                        self._e = e_best[0]
                        if len(e_best) > 1:
                            # new starting, remove the others
                            e_best.pop()
                            a_best.pop()
        self._a = a_choice
        self._e = e_choice
        self._elist.append(e_choice)
        self.h5save_result()


class qite_1step_rdle(qite_1step):
    '''
    one-step qite with linear quation of reduced dimension for solving a.
    '''
    def get_b(self):
        c = numpy.sqrt(1 - 2*self._db*self._e)
        b = self._bi.copy()
        if self._a is not None:
            b -= self._a.dot(self._s)*2
        b /= c
        return b

    def setup_init_sb(self):
        # set S = <\Psi_0 | \sigma_I^\dagger \sigma_J | \Psi_0>
        nterms = len(self._ansatz_ops)
        s = numpy.zeros([nterms, nterms])
        for i, opi in enumerate(self._ansatz_ops):
            for j, opj in enumerate(self._ansatz_ops):
                op = opi*opj
                sij = self.get_expval_init_ps(op).real
                s[i, j] = sij
        self._s = s
        # fixed part: b = 1j*<\Psi_0 |H \sigma_I| \Psi_0> + c.c.
        b = numpy.zeros([nterms])
        for i, opi in enumerate(self._ansatz_ops):
            bisum = 0
            for term in self._hop:
                hsigma = term*opi
                bi = self.get_expval_init_ps(hsigma)
                bisum += bi
            b[i] = -bisum.imag*2
        self._bi = b


class qite_1step_cdle(qite_1step):
    '''
    one-step qite with linear quation of complete dimension for solving a.
    '''
    def get_b(self):
        c = numpy.sqrt(1 - 2*self._db*self._e)
        b = self._bi.copy()
        if self._a_full is not None:
            b -= self._a_full.dot(self._s)*2
        b /= c
        return b

    def get_updated_a(self):
        self._a_full = super().get_updated_a()
        # return the reduced one.
        a = self._a_full[self._ansatz_ops_indices]
        return a

    def setup_init_sb(self, tol=1.e-6):
        self._a_full = None
        # set S = <\Psi_0 | \sigma_I^\dagger \sigma_J | \Psi_0>
        nterms = 4**self._nq
        s = numpy.zeros([nterms, nterms])
        pauli_list = ["I","X","Y","Z"]
        # store pauli terms to avoid duplicated efforts.
        term_list = []
        from itertools import product
        for i, labeli in enumerate(product(pauli_list, repeat=self._nq)):
            if i == 0:
                tlabel = "1*I"
            else:
                tlabel = "1*"+"".join(f"{s}{j}" for j,s
                        in enumerate(labeli[::-1]) if s != "I")
            termi = PauliTerm.from_compact_str(tlabel)
            term_list.append(termi)
        self._complete_term_list = term_list

        if os.path.isfile("data_init.h5"):
            with h5py.File("data_init.h5", "r") as f:
                self._s = f["/s_matrix"][()]
                buf = f["/pauli_terms_expval"][()]
        else:
            buf = numpy.ones(nterms, dtype=numpy.complex) * 10
            for i, termi in enumerate(term_list):
                for j, termj in enumerate(term_list):
                    if j > i:
                        break
                    op = termi*termj
                    coef1 = op.coefficient
                    if abs(coef1) < tol:
                        continue
                    idx = get_pauli_term_index(op, self._nq)
                    if buf[idx] > 7:
                        # not evaluated before
                        sij = self.get_expval_init_ps(op)
                        buf[idx] = sij/coef1
                    else:
                        sij = buf[idx]*coef1
                    s[i, j] = sij.real
                    op = termj*termi  # termi and termj is hermitian individually
                    coef2 = op.coefficient
                    if abs(coef1 - coef2) < tol:
                        s[j, i] = sij.real
                    else:
                        s[j, i] = -sij.real
            self._s = s
        # fixed part: b = 1j*<\Psi_0 |H \sigma_I| \Psi_0> + c.c.
        b = numpy.zeros([nterms])
        for i, opi in enumerate(term_list):
            bisum = 0
            for term in self._hop:
                hsigma = term*opi
                idx = get_pauli_term_index(hsigma, self._nq)
                if buf[idx] > 7:
                    # not evaluated before
                    bi = self.get_expval_init_ps(hsigma)
                    buf[idx] = bi/hsigma.coefficient
                else:
                    bi = buf[idx]*hsigma.coefficient
                bisum += bi
            b[i] = -bisum.imag*2
        # save for recycle
        with h5py.File("data_init.h5", "w") as f:
            f["/pauli_terms_expval"] = buf
            f["/s_matrix"] = self._s
        self._bi = b


def get_pauli_term_index(term, nq):
    '''
    get the index of the pauli term in the full array.
    '''
    idx = 0
    oneindex = {"I": 0, "X": 1, "Y": 2, "Z": 3}
    for i in range(nq):
        s = term._ops.get(i, "I")
        idx += 4**i*oneindex[s]
    return idx

class qite_1step_wfn(qite_1step_cdle):
    '''
    using wavefunction simulator
    '''
    def file_input(self):
        super().file_input()
        from pygrisb.gsolver.forest.paramexpt import paramExperimentWFN
        self._param_expt = paramExperimentWFN(
                hamiltonian=self._hop,
                )
        # check exact energy
        print('Exact ground state energy:', self._param_expt.get_exact_gs())
        # load initial state
        self._param_expt.set_ref_state(qca.ref_states[self._imp])
        # print('initial circuit energy estimate: ', vqe.objective_function())

        # load ansatz
        ansatz_circuit = input_ansatz(self._imp)
        self._param_expt.set_ansatz(ansatz_circuit)

    def evaluate_e(self):
        self._e = self._param_expt.objective_function(self._a*self._db*(-2))
        self._elist.append(self._e)


def qite_cstep_wfn_from_file(tol=1.e-6):
    # read in Hamiltonian in text file.
    with open("h.inp", "r") as f:
        hop = [PauliTerm.from_compact_str(label.rstrip("\n")) for label in f
                if abs(float(label.split("*")[0])) >= tol]
    qite = qite_cstep_wfn(hop)
    qite.run()
