#!/usr/bin/env python
import os, numpy, sys, h5py
from pygrisb.gsolver.qite import qite_cstep_qc_from_file


qite = qite_cstep_qc_from_file()
qite.h5save_result()
