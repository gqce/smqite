from pyquil.quil import Program
from pyquil.gates import X
from pyquil.experiment._symmetrization import SymmetrizationLevel
from pyquil.experiment._calibration import CalibrationMethod
from pyquil.api import get_qc


qc = get_qc("Aspen-4-2Q-H-qvm")
cq = [0, 1]
shots = 2**10
verbose = False
maxiter = 20
db = 0.2
# ref states
ref_states = [
        [
        Program(X(cq[1]))  # 01
        ],
        ]
# ref_bitstrings should be consistent with ref_states
ref_bitstrings = [
        "10",
        ]
# the ansatz
ansatz_params = [
        [0],
        ]

ansatz_ops = [
        [
        "1*Y0X1",
        ],
        ]
